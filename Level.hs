module Level (
  level,
  laby,
  powerups,
  monsters,
  setLevel,
  setLaby,
  setPowerup,
  delPowerup,
  setMonster,
  delMonster
) where

import Utils
import Labyrinth
import MonsterMod

import Control.Monad.State
import qualified Data.HashMap.Lazy as HM
import qualified Data.Set as S
import qualified Data.List as L

-- Get current map from environment
laby :: State Env Laby
laby = getX l_laby

-- Get powerups from the current level
powerups :: State Env (HM.HashMap Pos Powerup)
powerups = getX l_powerups

-- Get monsters on the current level
monsters :: State Env (S.Set Monster)
monsters = getX l_monsters

-- Not exported, just simplifies the getters
getX :: (Level -> b) -> State Env b
getX f = do
  lvl <- level
  return $ f lvl

-- Sets the current level
setLevel :: Level -> State Env ()
setLevel x = do
  s <- get
  modify (\s -> s{levels = HM.insert (levelN s) x (levels s)})

-- Sets the current labyrinth
setLaby :: Laby -> State Env ()
setLaby x = do
  lvl <- level
  setLevel lvl{l_laby = x}

-- Sets a new powerup (or overwrites an old powerup)
setPowerup :: Pos -> Powerup -> State Env ()
setPowerup p x = do
  lvl <- level
  pws <- powerups
  setLevel lvl{l_powerups = HM.insert p x pws}

-- Deletes a powerup from the current level
delPowerup :: Pos -> State Env ()
delPowerup p = do
  lvl <- level
  pws <- powerups
  setLevel lvl{l_powerups = HM.delete p pws}
  
-- Sets a monster on the current level
setMonster :: Monster -> State Env ()
setMonster x = do
  lvl <- level
  mons <- monsters
  setLevel lvl{l_monsters = S.insert x mons}

-- Deletes a monster from the current level
delMonster :: Monster -> State Env ()
delMonster x = do
  lvl <- level
  mons <- monsters
  setLevel lvl{l_monsters = S.delete x mons}

-- Get current level from environment
level :: State Env Level
level = do
  s <- get
  let maybeLevel = HM.lookup (levelN s) (levels s)
  maybe newLevel return maybeLevel

-- Create new level and insert it into the environment
newLevel :: State Env Level
newLevel = do         
  s <- get
  let n = levelN s
  laby <- generateLaby n
  pwrps <- generatePowerups n laby
  mnstrs <- generateMonsters (abs n) laby
  let lvl = Level laby mnstrs pwrps
  modify (\s -> s{levels = HM.insert n lvl $ levels s})
  return lvl

-- Generates monsters for a new level
generateMonsters :: Int -> Laby -> State Env (S.Set Monster)
generateMonsters (-3) _ = return S.empty
generateMonsters n lab = do
  s <- get
  mon <- generateMonster n lab 
  mons <- generateMonsters (n-1) lab
  return $ S.insert mon mons

-- Generates a single monster
generateMonster :: Int -> Laby -> State Env Monster
generateMonster n lab = do
  s <- get
  pos <- randomPos lab Empty
  let t = time s
  rnd <- takeRandomInt (0,2)
  case rnd of
    0 -> monsterCreator n pos t "Evil Vampire" (color 1 $ "V") 10 20 10 20
    1 -> monsterCreator n pos t "Rabid Bat" (color 1 $ "B") 5 10 5 10
    2 -> monsterCreator n pos t "Giant Slug" (color 3 $ "S") 20 40 20 20

-- Creates a monster from given name and stats
monsterCreator :: Int -> Pos -> Integer -> String -> String -> Int -> Int -> Int -> Int -> State Env Monster
monsterCreator n pos t name symbol as sp ad he = do
  s <- get
  rs <- takeRandomInts 4
  let d = 1 + abs n
      attackSpeed = toInteger $ (as `div` d) + ((rs !! 0) `mod` as) + 1
      speed = toInteger $ (sp `div` d) + ((rs !! 1) `mod` sp) + 1
      attackDmg = toInteger $ ((rs !! 2) `mod` ad) + 1
      health = toInteger $ he + d + ((rs !! 3) `mod` he)
  return $ Monster t attackSpeed speed pos name symbol attackDmg health

-- Generates powerups for a new level
generatePowerups :: Int -> Laby -> State Env (HM.HashMap Pos Powerup)
generatePowerups n lab = do
  s <- get
  posH <- randomPos lab Empty
  posM <- randomPos lab Empty
  let hp = Powerup Health 30
      mp = Powerup Mana 30
      hash = HM.fromList [(posH, hp), (posM, mp)]
  return hash
