module Utils (
  Pos(..),
  Player(..),
  Powerup(..),
  Power(..),
  Monster(..),
  Laby(..),
  Cell(..),
  Level(..),
  Env(..),
  takeRandomInt,
  takeRandomInts,
  bold,
  highlight,
  color
) where

-- Requires a somewhat recently package: unordered-container
import qualified Data.HashMap.Lazy as HM
import qualified Data.Set as S
import Control.Monad.State
import System.Info
import System.Random

-- This is the top level object containing all state of the program.
data Env = Env {
  -- The floor number which the player is on currently
  levelN :: Int,
  -- This is a hashmap of the different floors the which can be 
  --expanded both up and down
  levels :: HM.HashMap Int Level,
  -- The player
  player :: Player,
  -- The time
  time :: Integer,
  -- A generator used to generate levels and monsters
  gene :: StdGen
}

-- All data about the player
data Player = Player {
  -- (x,y) cordinates of the players position
  position :: Pos,
  -- If health reaches 0 the player dies
  health :: Integer,
  -- mana increases damage and can be used to throw fireballs
  mana :: Integer
}

-- A position is usually in the format (x, y)
-- x increasing to the right and y increasing downwards
type Pos = (Int, Int)

-- Everything contained in a level
data Level = Level {
  l_laby :: Laby,
  l_monsters :: S.Set Monster,
  l_powerups :: HM.HashMap Pos Powerup
}

-- A labyrinth (all lists should be of equal length)
data Laby = Laby {
  cells :: [[Cell]]
}

-- A cell is either a wall (can't walk there), empty (can walk there)
-- or has an entity (used for printing).
data Cell = Wall | Empty |
            StairUp | StairDown |
            Entity String deriving (Eq)

-- All data about a powerup
data Powerup = Powerup {
  power :: Power,
  strength :: Integer
}

-- Types of powerups
data Power = Health | Mana
  deriving Eq

-- All data about a monster
data Monster = Monster {
  m_time :: Integer,
  m_attackSpeed :: Integer,
  m_speed :: Integer,
  m_position :: Pos,
  m_name :: String,
  m_symbol :: String,
  m_attackDmg :: Integer,
  m_health :: Integer
} deriving (Show, Eq)

-- Returns a random integer and replaces the generator in the Env 
takeRandomInt :: (Int,Int) -> State Env Int
takeRandomInt range = do
  s <- get
  let (r,newGen) = randomR range $ gene s
  modify (\s -> s{gene = newGen})
  return r

-- Returns a list random integers and replaces the generator in the Env
takeRandomInts :: Int -> State Env [Int]
takeRandomInts n = takeRandomInts' n []
  where takeRandomInts' :: Int -> [Int] -> State Env [Int]
        takeRandomInts' 0 list = return list
        takeRandomInts' n list = do
          s <- get
          let (r,newGen) = random $ gene s
          modify (\s -> s{gene = newGen})
          takeRandomInts' (n-1) (r:list)
  
-- Text functions
bold text | os == "mingw32" = text
          | otherwise  = "\ESC[1m" ++ text ++ "\ESC[0m\ESC[37m\ESC[40m"
highlight text | os == "mingw32" = text
               | otherwise = "\ESC[7m" ++ text ++ "\ESC[27m"
color int text | os == "mingw32" = text
               | otherwise = "\ESC[" ++ (show $ 30 + int) ++
                   "m" ++ text ++ "\ESC[37m\ESC[40m"
