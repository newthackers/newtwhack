module GameLogic (
  newEnv,
  takeStep,
  KKey(..)
) where
import Control.Monad.State
import System.Random

import PlayerMod
import Labyrinth
import Utils
import MonsterMod
import Level

import qualified Data.HashMap.Lazy as HM
import qualified Data.Set as S
import qualified Data.List as L
import Data.List ((\\))

-- An empty environment
newEnv :: StdGen -> Env
newEnv gen = Env 0 HM.empty player 0 gen
  where player = Player (1,1) 100 50

-- Keyboard keys
data KKey = KUp | KDown | KLeft | KRight | KSpace | KOther deriving (Eq)

-- Takes a step in the world.
-- The player does its action and then does everything the monsters will do.
takeStep :: KKey -> State Env String
takeStep k = do
  str <- takePlayerAction k
  str2 <- takeMonsterAction
  return $ str ++ str2

-- Performs the players action.
takePlayerAction :: KKey -> State Env String
takePlayerAction key = case key of
  KSpace -> takePlayerSpell key
  _      -> takePlayerMove key

-- Perfroms a spell
takePlayerSpell :: KKey -> State Env String
takePlayerSpell key = do
  s <- get
  mons <- monsters
  if mana (player s) < 20 || S.null mons 
  then
    -- time spent while not doing magic?
    return "Not enough mana! (or no monsters around)"
  else do
    -- TODO: get closest monster
    let m = head $ S.toList mons
    delMonster m
    useMana 20
    return $ m_name m ++ " was hit by a great ball of fire and disappeared."
  where monDist m p = distP m p
        distP (x1,y1) (x2,y2) = (x1-x2)^2+(y1-y2)^2

-- Performs a move/attack by the player
takePlayerMove :: KKey -> State Env String
takePlayerMove key = do
  s <- get
  let pos = position $ player s
      newPos = getNewPos pos key
      lev = levelN s
  lab <- laby
  mons <- monsters
  case L.find ((==newPos) . m_position) $ S.toList mons of
    Just m -> do
      modify (\s -> s{time = 10 + time s})
      playerDealDamage m
    Nothing -> if isWalkablePos lab newPos
      then do changePos newPos
              pwrs <- powerups
              checkPow newPos pwrs
              checkStairs lab newPos
              modify (\s -> s{time = 10 + time s})
              return ""
      else do
        modify (\s -> s{time = 3 + time s})
        return $ (bold "Bump!") ++ " You fumble and walk into the wall. "
  where checkPow p pows = maybe (return ()) (use p) (HM.lookup p pows) 
        use pos pow = do
          usePow pow
          delPowerup pos
  
  
-- Checks a position for stairs and updates the current level accordingly. 
checkStairs :: Laby -> Pos -> State Env ()
checkStairs l p
  | getCell l p == StairUp = do
      modify (\s -> s{levelN = levelN s + 1})
      timeTravelMonsters
  | getCell l p == StairDown = do
      modify (\s -> s{levelN = levelN s - 1})
      timeTravelMonsters
  | otherwise = return ()

-- Moves the monsters to the current time
timeTravelMonsters :: State Env ()
timeTravelMonsters = do
  lvl <- level
  mons <- monsters
  s <- get
  let t = time s
      f = (\m -> m{m_time = t + 1})
  setLevel lvl{l_monsters = S.map f mons}

-- Lets the player deal damage to a monster
playerDealDamage :: Monster -> State Env String
playerDealDamage m = do
  s <- get
  dmg <- randomDamage $ (mana $ player s) `div` 10
  takeDamage m dmg

-- Performs all monster actions
takeMonsterAction :: State Env String
takeMonsterAction = do
  mons <- monsters
  s <- get
  if S.null mons
    then return ""
    else do
      let monster = S.findMin mons
      if m_time monster < time s
        then do
          (newM, str) <- takeAction monster
          delMonster monster
          setMonster newM
          str2 <- takeMonsterAction
          return $ str ++ str2
        else return ""

-- Calculates the new position for the player
getNewPos :: Pos -> KKey -> Pos
getNewPos (x, y) KUp    = (x  , y-1)
getNewPos (x, y) KDown  = (x  , y+1)
getNewPos (x, y) KLeft  = (x-1, y  )
getNewPos (x, y) KRight = (x+1, y  )

-- Performs one action for a monster
takeAction :: Monster -> State Env (Monster, String)
takeAction m = do
  s <- get
  lab <- laby
  mons <- monsters
  let myPos = m_position m
      playerPos = position $ player s
      walkable = getWalkablePos lab \\ map m_position (S.toList mons)
  if isNearby myPos playerPos
    then do
      dmg <- randomDamage $ m_attackDmg m
      playerTakeDamage dmg
      let str = (m_name m) ++ " hit you for " ++ (show dmg)
              ++ " hitpoints!"
          new_m = m{m_time = m_time m + m_attackSpeed m}
      return (new_m, str)
    else do
      let newPos = wayFind walkable myPos playerPos
          newMonster = m{m_position = newPos,
                         m_time = m_time m + m_speed m}
      return (newMonster, "")
  where isNearby (x1, y1) (x2, y2) = abs (x2 - x1) <= 1
                                  && abs (y2 - y1) <= 1

-- Lets the monster take damage
takeDamage :: Monster -> Integer -> State Env String
takeDamage m dmg = do
  s <- get
  delMonster m
  let newM = m{m_health = m_health m - dmg}
  if m_health newM <= 0
    then do
      return $ "You killed the " ++ (m_name m) ++ ". "
    else do
      setMonster newM
      return $ "You dealt " ++ (show dmg) ++ " damage to the " ++
               (m_name newM) ++ ". "

-- Randomizes damage
randomDamage :: Integer -> State Env Integer
randomDamage base = do
  r <- takeRandomInt (0, fromInteger base)
  return (base + toInteger r)
