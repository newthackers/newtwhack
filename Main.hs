module Main (
  main
) where

import Data.Char
import Control.Monad.State
import System.Random
import System.IO
import System.Info

import GameLogic
import Labyrinth
import Utils
import Level

import qualified Data.HashMap.Lazy as HM
import qualified Data.Set as S

-- Copied from the internet.
{-# LANGUAGE ForeignFunctionInterface #-}
import Foreign.C.Types
getHiddenChar = fmap (chr.fromEnum) c_getch
foreign import ccall unsafe "conio.h getch"
  c_getch :: IO CInt

-- Fixes the getChar on WIndows
betterGetChar :: IO Char
betterGetChar = if os == "mingw32"
                  then getHiddenChar
                  else getChar

-- Runs the gameloop
main :: IO ()
main = do
  -- Disable echoing and buffering
  hSetEcho stdin False
  hSetBuffering stdin NoBuffering
  hSetBuffering stdout NoBuffering

  -- Create a new game
  gen <- newStdGen
  let e = newEnv gen
  let (str, e') = runState showWorld e

  --To remove keypress from view and make text white.
  putStrLn $ bold "Welcome to Newtwhack!"
  putStr str

  gameLoop e'
  putStrLn "\nGame over!"

-- gameLoop
gameLoop :: Env -> IO ()
gameLoop env = do
  move <- getMove
  -- Update the world.
  let (str, env') = runState (takeStep move) env
      (str2, env'') = runState showWorld env'
  putStrLn $ "\n" ++ str
  putStr str2
  if health (player env') < 0
    then return ()
    else gameLoop env''

-- Shows (and if necessary, generates) the world.
showWorld :: State Env String
showWorld = do
  lab <- laby
  pwrs <- powerups
  mons <- monsters
  env <- get
  let plr = player env
      pos = position plr
      lab1 = HM.foldlWithKey' addPowers lab pwrs
      lab2 = S.foldl addMonsters lab1 mons
      lab3 = update lab2 pos (Entity . bold $ color 5 "@" )
  return $ show lab3 ++ "Health: " ++ (show $ health plr) ++
                        " Mana: "  ++ (show $ mana plr) ++
                        " Floor: " ++ (show $ levelN env) ++
                        " Time: "  ++ (show $ time env)
  where addPowers lab pos p = update lab pos (Entity . bold $ color c "P")
          where c | power p == Health = 1
                  | otherwise         = 4
        addMonsters lab m = update lab (m_position m) (Entity $ m_symbol m)

-- Gets the pressed arrow-key.
getMove :: IO KKey
getMove = do
       c <- betterGetChar
       case c of
              'a' -> return KLeft
              'w' -> return KUp
              'd' -> return KRight
              's' -> return KDown
              ' ' -> return KSpace
              _  -> getMove
