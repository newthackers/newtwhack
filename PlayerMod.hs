module PlayerMod where

import Utils
import Control.Monad.State


-- Changes the position of the player
changePos :: Pos -> State Env ()
changePos pos = do
  s <- get
  modify (\s -> s{player = cp (player s) pos})
    where cp player pos = player{position = pos}

-- Uses a given power up on the player
usePow :: Powerup -> State Env ()
usePow powerup = do
  s <- get
  case power powerup of
    Health -> modify (\s -> s{player = cp (player s) (strength powerup)})
      where cp p str = p{health = health p + str}
    Mana -> modify (\s -> s{player = cp (player s) (strength powerup)})
      where cp p str = p{mana = mana p + str}

-- takes damage
playerTakeDamage :: Integer -> State Env ()
playerTakeDamage dmg = do
  s <- get
  let newHealt = health (player s) - dmg
      newPlayer = (player s){health = newHealt}
  modify (\env -> env{player = newPlayer})

-- use mana
useMana :: Integer -> State Env ()
useMana man = do
  s <- get
  let newMana = mana (player s) - man
      newPlayer = (player s){mana = newMana}
  modify (\env -> env{player = newPlayer})
