module MonsterMod (
  wayFind
) where

import Control.Monad.State
import qualified Data.List as L
import Data.List ((\\))
import Data.Ord
import qualified Data.Set as S

import Labyrinth
import Utils

instance Ord Monster where
  compare a b = comparing func a b
    where func a = (m_time a, m_speed a, m_health a, m_position a)

-- Walks directly towards the goal. Not very smart.
wayFind :: [Pos] -> Pos -> Pos -> Pos
wayFind walkable (sx, sy) (gx, gy) = newPos
  where newPos = if firstChoice `elem` walkable
                   then firstChoice
                   else if secondChoice `elem` walkable
                          then secondChoice
                          else (sx, sy)
        preferX = abs (sx - gx) >= abs (sy - gy)
        xchoice = (sx + signum (gx - sx), sy)
        ychoice = (sx, sy + signum (gy - sy))
        firstChoice = if preferX then xchoice else ychoice
        secondChoice = if preferX then ychoice else xchoice
