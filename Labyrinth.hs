module Labyrinth (
  newLaby,
  getCell,
  update,
  isWalkablePos,
  getWalkablePos, 
  generateLaby,
  randomPos
) where

import Data.List (intercalate)
import Data.Maybe (isJust)
import Control.Monad.State

import Utils

import qualified Data.HashMap.Lazy as HM
----------------------
-- Cell
----------------------

instance Show Cell where
  show x = case x of
    Entity c  -> c
    StairDown -> color 2 $ "v"
    StairUp   -> color 2 $ "^"
    Wall      -> "#"
    Empty     -> "."

-- Is it possible to stand on this type of cell
isWalkable Wall = False
isWalkable _    = True

----------------------
-- Labyrinth
----------------------

instance Show Laby where

  show (Laby lab) = intercalate "\n" $ map stringify lab
    where stringify = concat . map show 

-- Returns whether the player can stand on that position or not.
isWalkablePos :: Laby -> Pos -> Bool
isWalkablePos l p = isWalkable $ getCell l p

-- Returns the cell at position (x, y)
getCell :: Laby -> Pos -> Cell
getCell (Laby lab) (x, y) = (lab !! y) !! x

-- Generates an x by y rectangle of walls
fullRoom :: Int -> Int -> Laby
fullRoom x y = Laby . replicate y $ replicate x Wall

-- Creates an empty level
newLaby :: Int -> Int -> Laby
newLaby x y = completedLab
  where completedLab = placeRoom (fullRoom x y) (0, 0) (x-1, y-1)

-- Changes the cell at position (x, y)
update :: Laby -> Pos -> Cell -> Laby
update (Laby lab) (x, y) cell = Laby (lab !!= (y, newRow))
  where newRow = (lab !! y) !!= (x, cell)
  
-- Places a room with corners on the given pos
-- This is here so we can use it in future version of 
-- the coolest labyrinth generator ever.
placeRoom :: Laby -> Pos -> Pos -> Laby
placeRoom (Laby lab) p1@(x1, y1) p2@(x2, y2) = Laby $ [
           [ correctCell (x, y) | x <- [0..length (head lab) - 1]]
             |  y <- [0..length lab - 1]
         ]
  where correctCell (x, y) = if inRect (x, y)
          then Empty
          else getCell (Laby lab) (x, y)
        inRect (x, y) = x1 < x && x < x2 && y1 < y && y < y2

-- Returns a list of all walkable positions
getWalkablePos :: Laby -> [Pos]
getWalkablePos lab = [ (x, y) | y <- [0..21], x <- [0..79],
                                isWalkablePos lab (x, y) ]
-- Will crash if given an empty Laby
getSize lab = (length (cells lab !! 0), length (cells lab))

-- Return True if position is a wall or next to a wall.
nextToEmpty :: Laby -> Pos -> Bool
nextToEmpty lab pos@(m,n) | m<=0 || n<=0 || m>=x || n>=y = False
                          | otherwise = all ((== Empty) . getCell lab)
                              [(m+dx,n+dy) | dx <- [-1..1], dy <- [-1..1]]
                              where (x,y) = getSize lab

-- Returns a position of a wall in a Laby
-- OBS! Don't call with empty labyrinth, will result in infinite loop
randomPos :: Laby -> Cell -> State Env Pos
randomPos lab cell = do
  s <- get
  let (x,y) = getSize lab
  px <- takeRandomInt (0,x-1)
  py <- takeRandomInt (0,y-1)
  let p = (px,py)
  if getCell lab p == cell
    then
      return p
    else
      randomPos lab cell

-- lab :: Laby
-- dir should be one of (1,0) (-1,0) (0,-1) (0,1)
-- Adds a wall from pos n pieces long in direction dir
addWall :: Laby -> Pos -> (Int,Int) -> Pos -> Laby
addWall lab pos (n1,n2) dir = foldl update' lab posList
  where update' a b  = update a b Wall
        posList = [pos !+ (m !* dir) | m <- [n1..n2] ]

-- Checks how long you can make a wall without blocking a room
-- pos = starting pos
-- dir = direction of wall
getPossibleWall :: Laby -> Pos -> Int -> Pos -> Int
getPossibleWall lab pos n dir = if (nextToEmpty lab (pos !+ ((n+1) !* dir)))
                                then
                                  getPossibleWall lab pos (n+1) dir
                                else
                                  n

-- Creates an empty level
-- PROBLEM: doesn't check where it sets other stair
addStairs :: Laby -> Pos -> Cell -> State Env Laby
addStairs lab pos stair = do
  rpos <- randomPos lab Empty
  let lab' = update lab pos stair
      lab'' = update lab' rpos $ otherStair stair
  return lab''

-- Returns the stair which is not given.
otherStair :: Cell -> Cell
otherStair StairUp = StairDown
otherStair StairDown = StairUp

-- Generates a labyrinth
generateLaby :: Int -> State Env Laby
generateLaby n = do
  s <- get
  let emptyFloor = newLaby 80 22
      stair = if n > 0 then StairDown else StairUp
      playerPos = position $ player s
  floor <- addStairs emptyFloor playerPos stair
  floor' <- addWalls floor (40+20*abs n)
  return floor'

-- Adds walls until it gets tired
-- rs is list of random Ints
-- lab :: Laby
-- walls = will try to add a wall this many times
addWalls :: Laby -> Int -> State Env Laby
addWalls lab walls = do
  wp <- randomPos lab Wall
  let dirList = [(1,0), (-1,0), (0,-1), (0,1)]
      dir = until' ((1<) . getPossibleWall lab wp 1) dirList
      maxLeng = (maybe 1 (getPossibleWall lab wp 1) dir) - 1
  l2 <- takeRandomInt (1,maxLeng)
  l1 <- takeRandomInt (0,l2)
  let lab' = maybe lab (addWall lab wp (l1,l2)) dir
  if walls == 0
  then
    return lab'
  else
    addWalls lab' (walls-1)

-- Like until but uses a list instead of a function to generate values
-- If no value works then it returns Nothing
until' :: (a -> Bool) -> [a] -> Maybe a
until' f []      = Nothing
until' f (a:ass) = if f a
                   then
                     Just a
                   else
                     until' f ass

----------------------
-- Helpers
----------------------
(!!=) :: [a] -> (Int,a) -> [a]
[]       !!= _         = [] --error "Error: empty list"
(l:list) !!= (0,value) = value:list
(l:list) !!= (n,value) = l:(list !!= (n-1,value))
(!+) :: Pos -> Pos -> Pos
(x1,y1) !+ (x2,y2) = (x1+x2,y1+y2)

(!*) :: Int -> Pos -> Pos
x !* (x2,y2) = (x*x2,x*y2)

